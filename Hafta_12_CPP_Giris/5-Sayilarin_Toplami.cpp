/*
12-14 satırlarındaki kod bloğu şu şekilde de yazılabilir;
int sayi1,
	sayi2,
	toplam;
*/
#include <iostream>
using namespace std;
int main()
{
	//Degişken Deklarasyonu
	int sayi1;//Kullanıcıdan Alınacak 1. Sayı
	int sayi2;//Kullanıcıdan Alınacak 2. Sayı
	int toplam;//Sayıların Toplamı

	//Kullanıcıdan Sayıları Temin Et.
	cout << "Lutfen 1. Sayiyi Giriniz: ";
	cin >> sayi1;
	cout << "Lutfen 2. Sayiyi Giriniz: ";
	cin >> sayi2;

	//Sayıları Topla
	toplam = sayi1 + sayi2;

	//Sonucu Ekrana Yazdır.
	cout << "Sayilarin Toplami: " << toplam << endl;
	return 0;
}