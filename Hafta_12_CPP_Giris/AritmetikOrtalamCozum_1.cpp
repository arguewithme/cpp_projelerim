#include <iostream>
using namespace std;
int main()
{
	//Degişken Deklarasyonu
	int sayi1,
		sayi2,
		sayi3,
		toplam,
		ortalama;

	//Kullanıcıdan Sayıları Temin Et.
	cout << "Lutfen 1. Sayiyi Giriniz: ";
	cin >> sayi1;
	cout << "Lutfen 2. Sayiyi Giriniz: ";
	cin >> sayi2;
	cout << "Lutfen 3. Sayiyi Giriniz: ";
	cin >> sayi3;

	//Sayıları Topla
	toplam = sayi1 + sayi2 + sayi3;
	//Ortalamayı Hesapla
	ortalama = toplam / 3;
	//Sonucu Ekrana Yazdır.
	cout << "Sayilarin Toplami: " << ortalama << endl;
	return 0;
}