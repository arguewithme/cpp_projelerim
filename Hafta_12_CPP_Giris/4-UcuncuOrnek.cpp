//UcuncuOrnek.cpp
//Kullanıcıdan Aldığı 2 Tamsayıyı Toplayan Program
#include <iostream>//cout ve cin Kullanımları İçin Gerekli
using namespace std;//Standart Kütüphane
int main()
{
	//Değişken Deklarasyonu
	int sayi1,//Birinci Sayı
		sayi2,//İkinci Sayı
		toplam;//Girilen 2 Sayının Toplamını Saklayan Değişken
	cout << "Birinci Sayiyi Giriniz:";//Ekrana Mesaj Yaz
	cin >> sayi1;//Girilen Sayıyı sayi1 e Ata
	cout << "Ikinci Sayiyi Giriniz:";//Ekrana Mesaj Yaz
	cin >> sayi2;//Girilen Sayıyı sayi2 ye Ata
	toplam = sayi1 + sayi2;//Girilen 2 Sayıyı Topla
	cout << "Girilen 2 Sayinin Toplami:" << toplam << endl;//Ekrana Toplamı Yazdır
	return 0;//Programı Sonlandır
}