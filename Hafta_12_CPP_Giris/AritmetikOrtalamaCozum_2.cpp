#include <iostream>
using namespace std;
int main()
{
	//Degişken Deklarasyonu
	int sayi1;
	int sayi2;
	int	sayi3;
	float ortalama;

	//Kullanıcıdan Sayıları Temin Et.
	cout << "Lutfen 1. Sayiyi Giriniz: ";
	cin >> sayi1;
	cout << "Lutfen 2. Sayiyi Giriniz: ";
	cin >> sayi2;
	cout << "Lutfen 3. Sayiyi Giriniz: ";
	cin >> sayi3;

	//Ortalamayı Hesapla
	/*Pay ve Payda Integer ise Tam Sayı Olarak Böler. 
	Bunu Aşmak İçin Paydayı .0 Gibi Bir Ondalıklıya Çevir.
	*/

	ortalama = (sayi1 + sayi2 + sayi3) / 3.0;

	/*Diğer Bir Yöntem İse Payın Başına (float) ekleyebiliriz.
	float(sayi1+sayi2+sayi)
	//Sonucu Ekrana Yazdır.
	cout << "Sayilarin Toplami: " << ortalama << endl;
	return 0;
}