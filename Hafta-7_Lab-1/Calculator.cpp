#include <iostream>
using namespace std;

class Calculator
{
	float a, b;
	public:
		float topla(float, float);
		float cikar(float, float);
		float carp(float, float);
		float bol(float, float);
};

float Calculator::topla(float a, float b)
{
	return (a+b);
}

float Calculator::cikar(float a, float b)
{
	return (a-b);
}

float Calculator::carp(float a, float b)
{
	return (a*b);
}

float Calculator::bol(float a, float b)
{
	return (a/b);
}

int main()
{
	Calculator calc;
	float sonuc,a,b;
	char d;
	cout << "Lutfen matematiksel bir islem girin:(e.g. 1+2)" << endl;
	cin >> a >> d >> b;
	
	switch(d)
	{
		case '+':
			sonuc = calc.topla(a,b);
			cout << sonuc;
			break;
		case '-':
			sonuc = calc.cikar(a,b);
			cout << sonuc;
			break;
		case '*':
			sonuc = calc.carp(a,b);
			cout << sonuc;
			break;
		case '/':
			sonuc = calc.bol(a,b);
			cout << sonuc;
			break;
		default:
			cout << "Yanlis karakter girdiniz" << endl;
	}
}