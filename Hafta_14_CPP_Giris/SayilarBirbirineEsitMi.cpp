//DorduncuOrnek.cpp
#include <iostream>//cout ve cin Kullanımları İçin Gerekli
using namespace std;
int main()
{
	//Değişken Deklarasyonu
	int sayi1,
		sayi2;
	cout << "Lutfen 2 Sayi Giriniz\n"
		<< "Ve Ben Size Iliskilerini Soyleyeyim: ";
	cin >> sayi1 >> sayi2;//Kullanicidan 2 Sayi Al
	if (sayi1 == sayi2)
		cout << sayi1 << " Esittir " << sayi2 << endl;
	if (sayi1 != sayi2)
		cout << sayi1 << " Esit Degildir " << sayi2 << endl;
	if (sayi1<sayi2)
		cout << sayi1 << " Kucuktur " << sayi2 << endl;
	if (sayi1>sayi2)
		cout << sayi1 << " Buyuktur " << sayi2 << endl;
	return 0;//Programi Sonlandir
}



