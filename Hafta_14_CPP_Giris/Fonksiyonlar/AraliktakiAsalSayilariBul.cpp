//Bir Aralıktaki Asal Sayıları Bulan Program
#include <iostream>//cout ve cin
using namespace std;
bool AsalSayiMi(int);//Prototip
int main()
{
	int altLimit, ustLimit;
	cout << "Lutfen Alt ve Ust Limiti Giriniz: ";
	cin >> altLimit >> ustLimit;
	for (int i = altLimit; i <= ustLimit; i++)
	if (AsalSayiMi(i))
		cout << i << endl;
	return 0;
}
bool AsalSayiMi(int sayi)//Tanım
{
	if (sayi<2)
		return false;
	else
	{
		//Bir n Sayısının 2 ile sqrt(n) Arasında En Az
		//Bir Adet Pozitif Böleni Varsa Asal Sayı Olamaz 
		for (int i = 2; i*i <= sayi; i++)//i<=sqrt(sayi)
		if (sayi%i == 0)
			return false;
		return true;
	}
}


