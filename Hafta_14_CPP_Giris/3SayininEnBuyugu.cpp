#include <iostream>
using namespace std;
int main()
{
	//Değişken Deklerasyonu
	int sayi1, //Kullanıcıdan Alınan Birinci Sayı
		sayi2, //Kullanıcıdan Alınan İkinci Sayı
		sayi3, //Kullanıcıdan Alınan Üçüncü Sayı
		enBuyuk; //En Büyük Sayı

	//Kullanıcıdan Sayıları Temin Et
	cout << "Lütfen 3 Tam Sayı Giriniz: ";
	cin >> sayi1 >> sayi2 >> sayi3 << endl;

	//En Büyüğünü Bul
	enBuyuk = sayi1;
	if (sayi2 > enBuyuk)
	{
		enBuyuk = sayi2;
	}
	if (sayi3 > enBuyuk)
	{
		enBuyuk = sayi3;
	}

	//En Büyüğü Ekrana Yazdır
	cout << enBuyuk << endl;
	return 0;
}