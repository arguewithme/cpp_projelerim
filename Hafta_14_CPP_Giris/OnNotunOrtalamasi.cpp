/*BesinciOrnek.cpp
Sınıf Ortalamasını Sayaç Kontrollü (counter-controlled)
while Döngüsü İle Hesaplayan Program*/
#include <iostream>//cout ve cin Kullanabilmek İçin
using namespace std;
int main()
{
	//Değişken Deklarasyonu
	int toplam,   //Girilen Notların Toplamı
		notSayaci,//Girilen Notların Sayısı
		not,      //Girilen Tek Bir Notun Değeri
		ortalama; //Girilen Notların Ortalaması
	//İlk Değer Atama (initialization) 
	toplam = 0;     //toplam değişkenini sıfırla
	notSayaci = 0;  //Sayacı Döngü İçin Hazırla
	//Notları Kullanıcıdan Al ve İşle
	while (notSayaci<10)//Döngüyü 10 Kere Çalıştır
	{
		cout << "Notu Girin: "; //Uyarıyı Ekrana Yazdır
		cin >> not;             //Notu Klavyeden Oku
		toplam = toplam + not;    //Girilen Notu toplam a Ekle
		notSayaci = notSayaci + 1;//Sayacı 1 Artır                         
	}
	ortalama = toplam / notSayaci;//Tamsayı Bölme
	cout << "Sinif Ortalamasi: " << ortalama << endl;
	return 0;
}


