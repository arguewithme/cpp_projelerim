#include <iostream>
using namespace std;
int main()
{
	//Degişken Deklarasyonu
	int sayi1;//Kullanıcıdan Alınacak 1. Sayı
	int sayi2;//Kullanıcıdan Alınacak 2. Sayı
	int bolum;
	int kalan;

	//Kullanıcıdan Sayıları Temin Et.
	cout << "Lutfen 1. Sayiyi Giriniz: ";
	cin >> sayi1;
	cout << "Lutfen 2. Sayiyi Giriniz: ";
	cin >> sayi2;

	//Sayıları Böl
	bolum = sayi1/sayi2;
	kalan = sayi1%sayi2;
	//Sonucu Ekrana Yazdır.
	cout << "Sayilarin Bolumu: " << bolum << endl;
	cout << "Bolumlerinden Kalan: " << kalan << endl;
	return 0;
}