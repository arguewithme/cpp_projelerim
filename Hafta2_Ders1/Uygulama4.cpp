/*YedinciOrnek.cpp
Sınıf Ortalamasını Bitiş Değer Kontrollü
while Döngüsü İle Hesaplayan Program*/
#include <iostream>//cout ve cin Kullanabilmek İçin
#include <iomanip>//setprecision Kullanımı İçin
using namespace std;
int main()
{
//Değişken Deklarasyonu
int toplam,      //Girilen Notların Toplamı
notSayaci,   //Girilen Notların Sayısı
not;         //Girilen Tek Bir Notun Değeri
double ortalama; //Girilen Notların Ortalaması
    //İlk Değer Atama (initialization) 
toplam=0;     //toplam değişkenini sıfırla
    notSayaci=0;  //Sayacı Döngü İçin Hazırla
    cout<<"Notu Gir ya da -1 Yazarak Cik: ";
cin>>not;
while(not!=-1)//Bitiş Değeri -1
{
toplam=toplam+not;    //Girilen Notu toplam a Ekle
notSayaci=notSayaci+1;//Sayacı 1 Artır
cout<<"Notu Gir ya da -1 Yazarak Cik: ";
cin>>not;                         
}
ortalama=static_cast<double>(toplam)/notSayaci;//Ondalıklı Bölme
//ortalama=(double)toplam/notSayaci;//Ondalıklı Bölme
cout<<"Sinif Ortalamasi: "<<setprecision(3)<<ortalama<<endl;
return 0;                                        
}
