/*AltinciOrnek.cpp
Sınıf Ortalamasını Bitiş Değer Kontrollü
while Döngüsü İle Hesaplayan Program*/
#include <iostream>//cout ve cin Kullanabilmek İçin
using namespace std;
int main()
{
//Değişken Deklarasyonu
int toplam,   //Girilen Notların Toplamı
notSayaci,//Girilen Notların Sayısı
note,      //Girilen Tek Bir Notun Değeri
ortalama; //Girilen Notların Ortalaması
    //İlk Değer Atama (initialization) 
toplam=0;     //toplam değişkenini sıfırla
    notSayaci=0;  //Sayacı Döngü İçin Hazırla
    cout<<"Notu Gir ya da -1 Yazarak Cik: ";
cin>>note;
while(note!=-1)
{
toplam=toplam+note;    //Girilen Notu toplam a Ekle
notSayaci=notSayaci+1;//Sayacı 1 Artır
cout<<"Notu Gir ya da -1 Yazarak Cik: ";
cin>>note;                         
}
if (notSayaci==0)
    ortalama=0;
else
ortalama=toplam/notSayaci;//Tamsayı Bölme
cout<<"Sinif Ortalamasi: "<<ortalama<<endl;
return 0;                                        
}

