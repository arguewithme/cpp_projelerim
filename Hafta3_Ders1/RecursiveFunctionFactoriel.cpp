#include <iostream> 
using namespace std;
int Faktoriyel(int);//Fonksiyon Prototipi
int main()
{ 
for(int i=0;i<=10;i++)
cout<<i<<" != "<<Faktoriyel(i)<<endl;
return 0;  
}
//Rekürsif Faktöriyel Hesaplayan Fonksiyonun Tanımı
int Faktoriyel(int sayi)
{
if(sayi<=1)//Temel Durum
return 1;
else       //Rekürsif (Genel) Durum
return sayi*Faktoriyel(sayi-1);
}